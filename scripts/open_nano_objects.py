import argparse
import awkward as ak
import numpy as np

from coffea.nanoevents import NanoEventsFactory, NanoAODSchema, BaseSchema
from coffea.nanoevents.methods import candidate

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("inp", help="Input root file that you want to load")
    parser.add_argument("--treename", default="EventTupler/DeepJetvars", help="Treename to load")
    args = parser.parse_args()

    #"/net/scratch/NiclasEich/BTV/bpix_studies/BTVtuples_Run3/run2023C.root",
    nano = NanoEventsFactory.from_root(
        args.inp,
        schemaclass=BaseSchema,
        treepath=args.treename
    )
    events = nano.events()

    # fmt: off
    print(f"Entering debug in: {__file__}")
    from IPython import embed;embed()
    # fmt: on