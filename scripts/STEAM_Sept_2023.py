import argparse
import glob
import json
import os
import awkward as ak
import numpy as np
from coffea import processor
from coffea.nanoevents import NanoAODSchema
from utils.processors import ExportProcessor, TriggerReviewProcessor
from utils.plotting import plot_efficiency,plot_histo,plot_efficiencies
from utils.utils import getError

def build_default_fileset(base_path):
    fileset = {}
    fileset['Run2023C'] = glob.glob('{}/MuonEG_Run2023C_comb/*.root'.format(base_path))
    # fileset['Run2023C'] = glob.glob('{}/MuonEG_Run2023C_model/*.root'.format(base_path))
    fileset['Run2023D'] = glob.glob('{}/MuonEG_Run2023D_comb/*.root'.format(base_path))
    # fileset['Run2022C'] = glob.glob('{}/MuonEG_Run2022C/*.root'.format(base_path))
    # fileset['Run2022D'] = glob.glob('{}/MuonEG_Run2022D_comb/*.root'.format(base_path))
    # fileset['Run2022E'] = glob.glob('{}/MuonEG_Run2022E/*.root'.format(base_path))
    # fileset['Run2022F'] = glob.glob('{}/MuonEG_Run2022F/*.root'.format(base_path))
    # fileset['Run2022G'] = glob.glob('{}/MuonEG_Run2022G/*.root'.format(base_path))
    # with open("AutoMake_Run3_2023_Sept_vs2022.json","w") as f:
    # with open("AutoMake_Run3_2023_CvsD.json","w") as f:
    # with open("AutoMake_Run3_2022vs2023_2024Review.json","w") as f:
    with open("AutoMake_Run3_2023Cvs2023D_2024Review.json","w") as f:
        json.dump(fileset,f)
    return fileset

if __name__ == "__main__":
    """
    Read in args
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--debug", action="store_true", help="Activate debug settings")
    parser.add_argument("--out", default="plots_Run3_2022vs2023_2024Review/", help="plot output name")
    parser.add_argument("--fileset", default=None, help="Json to the fileset that you are using")
    parser.add_argument("--filedir", default="/afs/cern.ch/user/l/lichengz/public/BTV/Commissioning/", help="Direction to the fileset that you are using, will generate Json file start with Auto")
    args = parser.parse_args()

    print("Args:")
    print(args)

    os.makedirs(args.out, exist_ok=True)

    print("Reading in sequences.json")
    with open("sequences.json", "r") as sequence_json:
        sequences = json.load(sequence_json)["sequences"]

    if args.fileset is None:
        fileset = build_default_fileset(args.filedir)
        print("Reading in fileset: AutoMake_Run3_2022vs2023_2024Review.json")
    else:
        print("Reading in fileset: {}".format(args.fileset))
        with open(args.fileset, "r") as fileset_json:
            fileset = json.load(fileset_json)

    # exit(0)
    
    kwargs = {}
    if args.debug:
        kwargs.update({"maxchunks": 1})
        for key in fileset.keys():
            fileset[key] = fileset[key][:1]

    iterative_run = processor.Runner(
        executor=processor.FuturesExecutor(compression=None, workers=10), schema=NanoAODSchema, **kwargs
    )
    paths = [
                "PFHT330PT30_QuadPFJet_75_60_45_40_TriplePFBTagDeepJet_4p5",
                "PFHT330PT30_QuadPFJet_75_60_45_40",
                "Mu12_DoublePFJets40MaxDeta1p6_DoublePFBTagDeepJet_p71",
                "IsoMu24",
                "PFHT400_FivePFJet_100_100_60_30_30_DoublePFBTagDeepJet_4p5",
                "PFHT400_FivePFJet_100_100_60_30_30",
                # "PFHT450_SixPFJet36",
                # "PFHT450_SixPFJet36_PFBTagDeepJet_1p59",
                # "QuadPFJet70_50_40_30",
                # "QuadPFJet70_50_40_30_PFBTagParticleNet_2BTagSum0p65",
    ] 
    
    Lumi_mask_jsondict={}
    
    for key in fileset.keys():
        if key == 'Run2022C':
            Lumi_mask_jsondict[key]="configs/Cert_Collisions2022_eraC_355862_357482_Golden.json"
        elif key == 'Run2022D':
            Lumi_mask_jsondict[key]="configs/Cert_Collisions2022_eraD_357538_357900_Golden.json"
        elif key == 'Run2022E':
            Lumi_mask_jsondict[key]="configs/Cert_Collisions2022_eraE_359022_360331_Golden.json"
        elif key == 'Run2022F':
            Lumi_mask_jsondict[key]="configs/Cert_Collisions2022_eraF_360390_362167_Golden.json"
        elif key == 'Run2022G':
            Lumi_mask_jsondict[key]="configs/Cert_Collisions2022_eraG_362433_362760_Golden.json"
        elif key == 'Run2023C':
            Lumi_mask_jsondict[key]="configs/Cert_Collisions2023_eraC_367095_368823_Golden.json"
        elif key == 'Run2023D':
            Lumi_mask_jsondict[key]="configs/Cert_Collisions2023_eraD_369803_370790_Golden.json"
        else:
            continue
    
    Lumi_mask_jsondict={
        'Run2022C': "configs/Cert_Collisions2022_eraC_355862_357482_Golden.json",
        'Run2022D': "configs/Cert_Collisions2022_eraD_357538_357900_Golden.json",
        'Run2022E': "configs/Cert_Collisions2022_eraE_359022_360331_Golden.json",
        'Run2022F': "configs/Cert_Collisions2022_eraF_360390_362167_Golden.json",
        'Run2022G': "configs/Cert_Collisions2022_eraG_362433_362760_Golden.json",
        'Run2023C': "configs/Cert_Collisions2023_eraC_367095_368823_Golden.json",   
        'Run2023D': "configs/Cert_Collisions2023_eraD_369803_370790_Golden.json",   
    }
    
    merge_datasets = {
        # "2022": ["Run2022C", "Run2022D", "Run2022E", "Run2022F", "Run2022G"],
        # "2023": ["Run2023C", "Run2023D"],
        "pre-BPix": ["Run2023C"],
        "post-BPix": ["Run2023D"],
        }
    
    comparisons = {
        # "all": ["2022","2023","pre-BPix","post-BPix"],
        # "2022vs2023":  ["2022","2023"],
        "BPix": ["pre-BPix","post-BPix"],
    }
    
    plot_config = {
                "2022": {"color": "#950008", "marker": "H"},
                "2023": {"color": "#016269", "marker": "P"},
                "pre-BPix": {"color": "#D05426", "marker": "P"},
                "post-BPix": {"color": "#322E95", "marker": "X"},
    }

    out = iterative_run(
        fileset,
        treename="Events",
        processor_instance=TriggerReviewProcessor(
            paths=paths,
            PU_file="configs/pileup_2022_310123.json",
            merge_datasets=merge_datasets,
            Lumi_mask_dict=Lumi_mask_jsondict,
        ),
    )
    
#     fileset_2023C = {}
#     fileset_2023D = {}
    
#     fileset_2023C["Run2023C"] = fileset["Run2023C"]
#     # fileset_2023D["Run2022G"] = fileset["Run2022G"]
#     fileset_2023D["Run2023D"] = fileset["Run2023D"]


#     out_2023C = iterative_run(
#         fileset_2023C,
#         treename="Events",
#         processor_instance=ExportProcessor(
#             sequences=sequences,
#             paths=paths,
#             PU_file="configs/pileup_2022_310123.json",
#             Lumi_mask_dict=Lumi_mask_jsondict,
#         ),
#     )
    
#     out_2023D = iterative_run(
#         fileset_2023D,
#         treename="Events",
#         processor_instance=ExportProcessor(
#             sequences=sequences,
#             paths=paths,
#             PU_file="configs/pileup_2022_310123.json",
#             Lumi_mask_dict=Lumi_mask_jsondict,
#         ),
#     )

    matching_sequences_paths = {
                                "PFHT330PT30_QuadPFJet_75_60_45_40_TriplePFBTagDeepJet_4p5": "hltBTagPFDeepJet4p5Triple",
                                "Mu12_DoublePFJets40MaxDeta1p6_DoublePFBTagDeepJet_p71": "hltBTagPFDeepJet0p71Double8Jets30",
                                "PFHT400_FivePFJet_100_100_60_30_30_DoublePFBTagDeepJet_4p5": "hltBTagPFDeepJet4p5Double",
                                # "PFHT450_SixPFJet36_PFBTagDeepJet_1p59": "hltBTagPFDeepJet1p59Single"
    }

    matching_references_paths = {
                                "PFHT330PT30_QuadPFJet_75_60_45_40_TriplePFBTagDeepJet_4p5": ["PFHT330PT30_QuadPFJet_75_60_45_40",3],
                                "Mu12_DoublePFJets40MaxDeta1p6_DoublePFBTagDeepJet_p71": ["IsoMu24",2],
                                "PFHT400_FivePFJet_100_100_60_30_30_DoublePFBTagDeepJet_4p5": ["PFHT400_FivePFJet_100_100_60_30_30",2],
                                # "PFHT450_SixPFJet36_PFBTagDeepJet_1p59": ["PFHT450_SixPFJet36",1],
    }

    N_bins = 12
    results_dict = {key: {} for key in merge_datasets.keys()}
    
    for merge_dataset in merge_datasets.keys():
        out_dir = os.path.join( args.out, merge_dataset)
        os.makedirs(out_dir, exist_ok=True)
        
        for path, values in matching_references_paths.items():
            results_dict[merge_dataset][path] = {}
            reference = values[0]
            index = values[1]
            
            mask_path = out[merge_dataset][path].value
            mask_reference = out[merge_dataset][reference].value
            
            print(f"Path:\t{path}")
            print(f"Reference:\t{reference}")
            
            npvs = np.array(out[merge_dataset]["npvs"].value)
            rec_lumi = np.array(out[merge_dataset]["recorded_lumi"].value)
            n_passing = mask_path & mask_reference
            n_total = mask_reference
            
            b_tag_values = np.array(out[merge_dataset]["btags"].value)[..., -index]
            jetphi_values = np.array(out[merge_dataset]["jetphi"].value)[..., -index]
            
            sanity_mask = (b_tag_values >= 0.) & np.invert(np.isinf(b_tag_values)) & np.invert( np.isnan(b_tag_values) ) 
            
            b_tag_values = b_tag_values[sanity_mask]
            jetphi_values = jetphi_values[sanity_mask]
            n_passing = n_passing[sanity_mask]
            n_total = n_total[sanity_mask]
            npvs = npvs[sanity_mask]
            rec_lumi = rec_lumi[sanity_mask]
            if merge_dataset == '2022':
                avg_inst_lumi = rec_lumi / 34.20
            else:
                avg_inst_lumi = rec_lumi / 26.60
                
            if merge_dataset == "2022":
                lumi_str = r"$34.20\,\mathrm{fb}^{-1}, 2022$($13.6\,\mathrm{TeV}$)"
            else:
                # lumi_str = r"$3.055\,\mathrm{fb}^{-1}, 2022,\ 26.6\,\mathrm{fb}^{-1}, 2023\ $($13.6\,\mathrm{TeV}$)"
                lumi_str = r"$26.6\,\mathrm{fb}^{-1}, 2023$($13.6\,\mathrm{TeV}$)"
                
            if index == 1:
                index_str = 'first'
            elif index == 2:
                index_str = 'second'
            elif index == 3:
                index_str = 'third'
                
            plot_texts = {
            "PFHT330PT30_QuadPFJet_75_60_45_40_TriplePFBTagDeepJet_4p5": \
                "PFHT > 330GeV\n4 PFJets 75, 60, 45, 40 > GeV\nJetHT > 330GeV\n3 jets passing DeepJet value > 0.24",
            "Mu12_DoublePFJets40MaxDeta1p6_DoublePFBTagDeepJet_p71": \
                "Muon pT > 12GeV\n2 PFJets 40, 40 > GeV\n2 PF Jets Delta Eta < 1.6\n2 jets passing DeepJet value > 0.86",
            "PFHT400_FivePFJet_100_100_60_30_30_DoublePFBTagDeepJet_4p5": \
                "PFHT > 400GeV\n5 PFJets pt > 100, 100, 60, 30, 30GeV\n2 jets passing DeepJet value > 0.29",
            "PFHT450_SixPFJet36_PFBTagDeepJet_1p59": \
                "PFHT > 450GeV\n6 PFJets pt > 36GeV\n1 jet passing DeepJet value > 0.45",
            }
        
            if path == "PFHT330PT30_QuadPFJet_75_60_45_40_TriplePFBTagDeepJet_4p5":
                plot_text = "PFHT > 330GeV\n4 PFJets 75, 60, 45, 40 > GeV\nJetHT > 330GeV\n3 jets passing DeepJet value > 0.24"
            elif path == "Mu12_DoublePFJets40MaxDeta1p6_DoublePFBTagDeepJet_p71":
                plot_text = "Muon pT > 12GeV\n2 PFJets 40, 40 > GeV\n2 PF Jets Delta Eta < 1.6\n2 jets passing DeepJet value > 0.86"
            elif path == "PFHT400_FivePFJet_100_100_60_30_30_DoublePFBTagDeepJet_4p5":
                plot_text = "PFHT > 400GeV\n5 PFJets pt > 100, 100, 60, 30, 30GeV\n2 jets passing DeepJet value > 0.29"
            elif path == "PFHT450_SixPFJet36_PFBTagDeepJet_1p59":
                plot_text = "PFHT > 450GeV\n6 PFJets pt > 36GeV\n1 jet passing DeepJet value > 0.45"
                
            """
            plot in b-tag bins
            """
            bins = np.linspace(0, 1., num=N_bins-1, endpoint=False)
            bin_digits = np.digitize(b_tag_values, bins)
            
            numerator = np.array([np.sum(n_passing[np.where(bin_digits == i)]) for i in range(N_bins)])
            denominator = np.array([np.sum(n_total[np.where(bin_digits == i)]) for i in range(N_bins)])
            
            efficiency = numerator/denominator
            lower_error, upper_error = getError(numerator, denominator)

            bin_centers = np.diff(np.concatenate((bins, [1.])))/2 + bins
            plot_title = " "
            ymax = np.min((1., np.max(efficiency[np.isfinite(efficiency)]) * 1.2))
            
            plot_efficiency(
                efficiency[1:], 
                lower_error[1:], upper_error[1:], 
                multiline_text = plot_text,
                bin_centers = bin_centers, 
                title=plot_title, 
                path=os.path.join(out_dir, f"{merge_dataset}_eff_b_tag.png"), 
                xlabel="{0} leading offline AK4 jet b-tagging score".format(index_str),
                ymax=ymax, 
                lumi = lumi_str,
                plot_config=plot_config, 
                label=merge_dataset
            )
            
            plot_efficiency(
                efficiency[1:], 
                lower_error[1:], upper_error[1:], 
                multiline_text = plot_text,
                bin_centers = bin_centers, 
                title=plot_title, 
                path=os.path.join(out_dir, f"{merge_dataset}_eff_b_tag_log.png"), 
                xlabel="{0} leading offline AK4 jet b-tagging score".format(index_str),
                log=True,
                ymax=ymax, 
                lumi = lumi_str,
                plot_config=plot_config, 
                label=merge_dataset
            )
            results_dict[merge_dataset][path]["b-tag"] = {"efficiency": efficiency[1:], "lower_error": lower_error[1:], "upper_error": upper_error[1:], "bin_centers": bin_centers}
            
            """
            plot in npvs 
            """
            bins = np.arange(15, 65, 5)
            bin_digits = np.digitize(npvs, bins)

            numerator = np.array([np.sum(n_passing[np.where(bin_digits == i)]) for i in range(len(bins))])
            denominator = np.array([np.sum(n_total[np.where(bin_digits == i)]) for i in range(len(bins))])

            efficiency = numerator / denominator
            lower_error, upper_error = getError(numerator, denominator)

            ymax = np.min((1., np.max(efficiency[np.isfinite(efficiency)]) * 1.2))
            bin_centers = np.diff(np.concatenate((bins, [bins[-1]+(bins[-1]-bins[-2])])))/2 + bins
            
            plot_efficiency(
                efficiency, 
                lower_error, upper_error, 
                multiline_text = plot_text,
                bin_centers = bin_centers, 
                title=plot_title, 
                path=os.path.join(out_dir, f"{merge_dataset}_eff_npv_{path}.png"), 
                xlabel="Number of reconstructed primary vertices",
                ymax=ymax, 
                plot_config=plot_config, 
                label=merge_dataset
            )
            
            plot_efficiency(
                efficiency, 
                lower_error, upper_error, 
                multiline_text = plot_text,
                bin_centers = bin_centers, 
                title=plot_title, 
                path=os.path.join(out_dir, f"{merge_dataset}_eff_npv_{path}_log.png"), 
                xlabel="Number of reconstructed primary vertices",
                log=True,
                ymax=ymax, 
                plot_config=plot_config, 
                label=merge_dataset
            )
            
            results_dict[merge_dataset][path]["npv"] = {"efficiency": efficiency, "lower_error": lower_error, "upper_error": upper_error, "bin_centers": bin_centers}
            
            """
            plot in jet phi 
            """
            bins = np.linspace(-3.2, 3.2, num=N_bins-1, endpoint=False)
            bin_digits = np.digitize(jetphi_values, bins)
            
            numerator = np.array([np.sum(n_passing[np.where(bin_digits == i)]) for i in range(N_bins)])
            denominator = np.array([np.sum(n_total[np.where(bin_digits == i)]) for i in range(N_bins)])
            
            efficiency = numerator/denominator
            lower_error, upper_error = getError(numerator, denominator)

            bin_centers = np.diff(np.concatenate((bins, [1.])))/2 + bins
            plot_title = " "
            ymax = np.min((1., np.max(efficiency[np.isfinite(efficiency)]) * 1.2))
            
            plot_efficiency(
                efficiency[1:], 
                lower_error[1:], upper_error[1:], 
                multiline_text = plot_text,
                bin_centers = bin_centers, 
                title=plot_title, 
                path=os.path.join(out_dir, f"{merge_dataset}_eff_jet_phi.png"), 
                xlabel="$\phi$ of AK4 jet with {0} leading offline b-tagging score".format(index_str),
                xmin=-3.2,
                xmax=3.2,
                ymax=ymax, 
                lumi = lumi_str,
                plot_config=plot_config, 
                label=merge_dataset
            )
            
            plot_efficiency(
                efficiency[1:], 
                lower_error[1:], upper_error[1:], 
                multiline_text = plot_text,
                bin_centers = bin_centers, 
                title=plot_title, 
                path=os.path.join(out_dir, f"{merge_dataset}_eff_jet_phi_log.png"), 
                xlabel="$\phi$ of AK4 jet with {0} leading offline b-tagging score".format(index_str),
                log=True,
                xmin=-3.2,
                xmax=3.2,
                ymax=ymax, 
                lumi = lumi_str,
                plot_config=plot_config, 
                label=merge_dataset
            )
            
            results_dict[merge_dataset][path]["jetphi"] = {"efficiency": efficiency[1:], "lower_error": lower_error[1:], "upper_error": upper_error[1:], "bin_centers": bin_centers}
            
            
    ## Comparison Plots
    
    for path, values in matching_references_paths.items():
        
        reference = values[0]
        index = values[1]
        if index == 1:
            index_str = 'first'
        elif index == 2:
            index_str = 'second'
        elif index == 3:
            index_str = 'third'
            
        if path == "PFHT330PT30_QuadPFJet_75_60_45_40_TriplePFBTagDeepJet_4p5":
            plot_text = "PFHT > 330GeV\n4 PFJets 75, 60, 45, 40 > GeV\nJetHT > 330GeV\n3 jets passing DeepJet value > 0.24"
        elif path == "Mu12_DoublePFJets40MaxDeta1p6_DoublePFBTagDeepJet_p71":
            plot_text = "Muon pT > 12GeV\n2 PFJets 40, 40 > GeV\n2 PF Jets Delta Eta < 1.6\n2 jets passing DeepJet value > 0.86"
        elif path == "PFHT400_FivePFJet_100_100_60_30_30_DoublePFBTagDeepJet_4p5":
            plot_text = "PFHT > 400GeV\n5 PFJets pt > 100, 100, 60, 30, 30GeV\n2 jets passing DeepJet value > 0.29"
        elif path == "PFHT450_SixPFJet36_PFBTagDeepJet_1p59":
            plot_text = "PFHT > 450GeV\n6 PFJets pt > 36GeV\n1 jet passing DeepJet value > 0.45"
            
        for comparison_name, comparison_runs in comparisons.items():
            out_dir = os.path.join(args.out, comparison_name)
            os.makedirs( out_dir, exist_ok=True )
            
            if "2022" in comparison_name:
                lumi_str = r"$34.20\,\mathrm{fb}^{-1}, 2022,\ 26.6\,\mathrm{fb}^{-1}, 2023\ $($13.6\,\mathrm{TeV}$)"
            else:
                lumi_str = r"$26.6\,\mathrm{fb}^{-1}, 2023$($13.6\,\mathrm{TeV}$)"
            
            """
            plot in b-tag bins
            """
            bin_centers = list(results_dict.values())[0][path]["b-tag"]["bin_centers"]
            plot_title = " "
            
            all_eff = np.concatenate( 
                    [ 
                        efficiency[np.isfinite(efficiency)] for efficiency in [results_dict[merge_dataset][path]["b-tag"]["efficiency"] for merge_dataset in comparison_runs]
                    ]
                )
            
            ymax = np.min((1., np.max(all_eff * 1.2)))
            plot_efficiencies(
                [results_dict[merge_dataset][path]["b-tag"]["efficiency"] for merge_dataset in comparison_runs],
                [results_dict[merge_dataset][path]["b-tag"]["lower_error"] for merge_dataset in comparison_runs],
                [results_dict[merge_dataset][path]["b-tag"]["upper_error"] for merge_dataset in comparison_runs],
                multiline_text = plot_text,
                bin_centers = bin_centers,
                labels=comparison_runs,
                plot_config=plot_config,
                title = plot_title, 
                ymax=ymax, 
                path=os.path.join(out_dir, f"eff_b-tag_{path}.png"), 
                xlabel="{0} leading offline AK4 jet b-tagging score".format(index_str), 
                log = False,
                lumi = lumi_str
            )
            
            plot_efficiencies(
                [results_dict[merge_dataset][path]["b-tag"]["efficiency"] for merge_dataset in comparison_runs],
                [results_dict[merge_dataset][path]["b-tag"]["lower_error"] for merge_dataset in comparison_runs],
                [results_dict[merge_dataset][path]["b-tag"]["upper_error"] for merge_dataset in comparison_runs],
                multiline_text = plot_text,
                bin_centers = bin_centers,
                labels=comparison_runs,
                plot_config=plot_config,
                title = plot_title, 
                ymax=ymax, 
                path=os.path.join(out_dir, f"eff_b-tag_{path}_log.png"), 
                xlabel="{0} leading offline AK4 jet b-tagging score".format(index_str), 
                log = True,
                lumi = lumi_str
            )
            
            """
            plot in npv bins
            """
            bin_centers = list(results_dict.values())[0][path]["npv"]["bin_centers"]
            plot_title = " "
            
            all_eff = np.concatenate( 
                    [ 
                        efficiency[np.isfinite(efficiency)] for efficiency in [results_dict[merge_dataset][path]["npv"]["efficiency"] for merge_dataset in comparison_runs]
                    ]
                )
            ymax = np.min( (1., np.max(all_eff * 1.2)) )
            plot_efficiencies(
                [results_dict[merge_dataset][path]["npv"]["efficiency"] for merge_dataset in comparison_runs],
                [results_dict[merge_dataset][path]["npv"]["lower_error"] for merge_dataset in comparison_runs],
                [results_dict[merge_dataset][path]["npv"]["upper_error"] for merge_dataset in comparison_runs],
                multiline_text = plot_text,
                bin_centers = bin_centers,
                labels=comparison_runs,
                plot_config=plot_config,
                title=plot_title, 
                ymax=ymax,
                path=os.path.join(out_dir, f"eff_npv_{path}.png"), 
                xlabel="Number of reconstructed primary vertices",
                log=False, 
                lumi = lumi_str
            )
            
            plot_efficiencies(
                [results_dict[merge_dataset][path]["npv"]["efficiency"] for merge_dataset in comparison_runs],
                [results_dict[merge_dataset][path]["npv"]["lower_error"] for merge_dataset in comparison_runs],
                [results_dict[merge_dataset][path]["npv"]["upper_error"] for merge_dataset in comparison_runs],
                multiline_text = plot_text,
                bin_centers = bin_centers,
                labels=comparison_runs,
                plot_config=plot_config,
                title=plot_title, 
                ymax=ymax,
                path=os.path.join(out_dir, f"eff_npv_{path}_log.png"), 
                xlabel="Number of reconstructed primary vertices",
                log=True, 
                lumi = lumi_str
            )
            
            """
            plot in jet phi bins
            """
            bin_centers = list(results_dict.values())[0][path]["jetphi"]["bin_centers"]
            plot_title = " "
            all_eff = np.concatenate( 
                    [ 
                        efficiency[np.isfinite(efficiency)] for efficiency in [results_dict[merge_dataset][path]["jetphi"]["efficiency"] for merge_dataset in comparison_runs]
                    ]
                )
            
            ymax = np.min((1., np.max(all_eff * 1.2)))
            plot_efficiencies(
                [results_dict[merge_dataset][path]["jetphi"]["efficiency"] for merge_dataset in comparison_runs],
                [results_dict[merge_dataset][path]["jetphi"]["lower_error"] for merge_dataset in comparison_runs],
                [results_dict[merge_dataset][path]["jetphi"]["upper_error"] for merge_dataset in comparison_runs],
                multiline_text = plot_text,
                bin_centers = bin_centers,
                labels=comparison_runs,
                plot_config=plot_config,
                title = plot_title, 
                xmin=-3.2,
                xmax=3.2,
                ymax=ymax, 
                path=os.path.join(out_dir, f"eff_jet_phi{path}.png"), 
                xlabel="$\phi$ of AK4 jet with {0} leading offline b-tagging score".format(index_str),
                log = False,
                lumi = lumi_str
            )
            
            plot_efficiencies(
                [results_dict[merge_dataset][path]["jetphi"]["efficiency"] for merge_dataset in comparison_runs],
                [results_dict[merge_dataset][path]["jetphi"]["lower_error"] for merge_dataset in comparison_runs],
                [results_dict[merge_dataset][path]["jetphi"]["upper_error"] for merge_dataset in comparison_runs],
                multiline_text = plot_text,
                bin_centers = bin_centers,
                labels=comparison_runs,
                plot_config=plot_config,
                title = plot_title, 
                xmin=-3.2,
                xmax=3.2,
                ymax=ymax, 
                path=os.path.join(out_dir, f"eff_jet_phi{path}_log.png"), 
                xlabel="$\phi$ of AK4 jet with {0} leading offline b-tagging score".format(index_str),
                log = True,
                lumi = lumi_str
            )
            
"""    

    
            

        
        reference = values[0]
        index = values[1]
        
        for comparison_name, comparison_runs in comparisons.items():
            out_dir = os.path.join(args.out, comparison_name)
            os.makedirs( out_dir, exist_ok=True )
        
        mask_path_2023C = out_2023C[path].value
        mask_path_2023D = out_2023D[path].value   
        
        mask_reference_2023C = out_2023C[reference].value
        mask_reference_2023D = out_2023D[reference].value
                
        try:
            seq = matching_sequences_paths[ path ]
        except KeyError:
            continue
        print("Path:\t{}".format(path))
        print("Reference:\t{}".format(reference))
        print("Seq:\t{}".format(seq))
                
        npvs_2023C = np.array(out_2023C["npvs"].value)
        npvs_2023D = np.array(out_2023D["npvs"].value)
        
        jetphi_2023C = np.array(out_2023C["jetphi"].value)[..., -index]
        print(jetphi_2023C)
        jetphi_2023D = np.array(out_2023D["jetphi"].value)[..., -index]
        
        # rec_lumi_2023 = np.array(out_2023["recorded_lumi"].value)
        # rec_lumi_2022 = np.array(out_2022["recorded_lumi"].value)

        n_passing_2023C = mask_path_2023C & mask_reference_2023C
        n_passing_2023D = mask_path_2023D & mask_reference_2023D
        
        n_total_2023C = mask_reference_2023C
        n_total_2023D = mask_reference_2023D
                
        b_tag_values_2023C = np.array(out_2023C["btags"].value)[..., -index]
        print(b_tag_values_2023C)
        b_tag_values_2023D = np.array(out_2023D["btags"].value)[..., -index]
                
        sanity_mask_2023C = (b_tag_values_2023C >= 0.) & np.invert(np.isinf(b_tag_values_2023C)) & np.invert( np.isnan(b_tag_values_2023C) )
        sanity_mask_2023D = (b_tag_values_2023D >= 0.) & np.invert(np.isinf(b_tag_values_2023D)) & np.invert( np.isnan(b_tag_values_2023D) )
        
        b_tag_values_2023C = b_tag_values_2023C[sanity_mask_2023C]
        b_tag_values_2023D = b_tag_values_2023D[sanity_mask_2023D]

        n_passing_2023C = n_passing_2023C[sanity_mask_2023C]
        n_passing_2023D = n_passing_2023D[sanity_mask_2023D]
        
        n_total_2023C = n_total_2023C[sanity_mask_2023C]
        n_total_2023D = n_total_2023D[sanity_mask_2023D]
        
        npvs_2023C = npvs_2023C[sanity_mask_2023C]
        npvs_2023D = npvs_2023D[sanity_mask_2023D]
        
        jetphi_2023C = jetphi_2023C[sanity_mask_2023C]
        jetphi_2023D = jetphi_2023D[sanity_mask_2023D]
        
        # rec_lumi_2023 = rec_lumi_2023[sanity_mask_2023]
        # rec_lumi_2022 = rec_lumi_2022[sanity_mask_2022]
        # avg_inst_lumi = rec_lumi / 23.31
        
        
        ##### plot in b-tag bins
        
        bins = np.linspace(0, 1., num=N_bins-1, endpoint=False)
        bin_digits_2023C = np.digitize(b_tag_values_2023C, bins)
        bin_digits_2023D = np.digitize(b_tag_values_2023D, bins)

        numerator_2023C = np.array([np.sum(n_passing_2023C[np.where(bin_digits_2023C == i)]) for i in range(len(bins))])
        numerator_2023D = np.array([np.sum(n_passing_2023D[np.where(bin_digits_2023D == i)]) for i in range(len(bins))])
        
        denominator_2023C = np.array([np.sum(n_total_2023C[np.where(bin_digits_2023C == i)]) for i in range(len(bins))])
        denominator_2023D = np.array([np.sum(n_total_2023D[np.where(bin_digits_2023D == i)]) for i in range(len(bins))])

        efficiency_2023C = numerator_2023C/denominator_2023C
        efficiency_2023D = numerator_2023D/denominator_2023D
        
        lower_error_2023C, upper_error_2023C = getError(numerator_2023C, denominator_2023C)
        lower_error_2023D, upper_error_2023D = getError(numerator_2023D, denominator_2023D)

        bin_centers = np.diff(np.concatenate((bins, [1.])))/2 + bins
        plot_title = path[:len(path) //2 ] + "\n" + path[len(path) //2 :]
        # ymax = np.min((1., np.max(efficiency_2023D[np.isfinite(efficiency_2023D)]) * 1.2))
        ymax = 1.2
        
        plot_efficiencies(
                [efficiency_2023C, efficiency_2023D], 
                [lower_error_2023C, lower_error_2023D],
                [upper_error_2023C, upper_error_2023D],
                plot_text,
                bin_centers,
                labels=['pre-BPix','post-BPix'],
                plot_config = plot_config,
                title = None, 
                ymax=ymax,
                path=os.path.join(args.out, "eff_b_tag_{}.png".format(seq)),
                xlabel="{0} leading offline AK4 jet b-tagging score".format(index_str),
                lumi = lumi_str
        )
        plot_efficiencies(
                [efficiency_2023C, efficiency_2023D],
                [lower_error_2023C, lower_error_2023D],
                [upper_error_2023C, upper_error_2023D],
                plot_text,
                bin_centers,
                labels=['pre-BPix','post-BPix'],
                plot_config = plot_config,
                title=None,
                path=os.path.join(args.out, "eff_b_tag_{}_log.png".format(seq)), 
                xlabel="{0} leading offline AK4 jet b-tagging score".format(index_str),
                ymax=ymax,
                lumi = lumi_str,
                log=True
        )
        
        plot_efficiencies(
                [efficiency_2023C, efficiency_2023D], 
                [lower_error_2023C, lower_error_2023D],
                [upper_error_2023C, upper_error_2023D],
                plot_text,
                bin_centers,
                labels=['pre-BPix','post-BPix'],
                plot_config = plot_config,
                title = None, 
                ymax=ymax,
                path=os.path.join(args.out, "eff_b_tag_{}.pdf".format(seq)),
                xlabel="{0} leading offline AK4 jet b-tagging score".format(index_str),
                lumi = lumi_str
        )
        plot_efficiencies(
                [efficiency_2023C, efficiency_2023D],
                [lower_error_2023C, lower_error_2023D],
                [upper_error_2023C, upper_error_2023D],
                plot_text,
                bin_centers,
                labels=['pre-BPix','post-BPix'],
                plot_config = plot_config,
                title=None,
                path=os.path.join(args.out, "eff_b_tag_{}_log.pdf".format(seq)), 
                xlabel="{0} leading offline AK4 jet b-tagging score".format(index_str),
                ymax=ymax,
                lumi = lumi_str,
                log=True
        )
        
#         plot_efficiency(
#             efficiency_2023C,
#             lower_error_2023C,
#             upper_error_2023C,
#             plot_text,
#             bin_centers,
#             label = None,
#             title = None,
#             ymax=ymax,
#             path=os.path.join(args.out, "2023_eff_b_tag_{}.png".format(seq)), 
#             xlabel="offline {} b-tag value".format(index),
#             lumi = r"$3.055\,\mathrm{fb}^{-1}, 2022, 26.6\,\mathrm{fb}^{-1}, 2023$ ($13.6\,\mathrm{TeV}$)",
#         )
        
#         plot_efficiency(
#             efficiency_2023C,
#             lower_error_2023C,
#             upper_error_2023C,
#             plot_text,
#             bin_centers,
#             label = None,
#             title = None,
#             ymax=ymax,
#             path=os.path.join(args.out, "2023_eff_b_tag_{}_log.png".format(seq)), 
#             log=True,
#             lumi = r"$3.055\,\mathrm{fb}^{-1}, 2022, 26.6\,\mathrm{fb}^{-1}, 2023$ ($13.6\,\mathrm{TeV}$)",
#             xlabel="offline {} b-tag value".format(index)
#         )
        
        ##### plot in npvs 
        
        bins = np.arange(10,70,5)
        
        bin_digits_2023C = np.digitize(npvs_2023C, bins)
        bin_digits_2023D = np.digitize(npvs_2023D, bins)
        
        numerator_2023C = np.array([np.sum(n_passing_2023C[np.where(bin_digits_2023C == i)]) for i in range(len(bins))])
        numerator_2023D = np.array([np.sum(n_passing_2023D[np.where(bin_digits_2023D == i)]) for i in range(len(bins))])
        
        denominator_2023C = np.array([np.sum(n_total_2023C[np.where(bin_digits_2023C == i)]) for i in range(len(bins))])
        denominator_2023D = np.array([np.sum(n_total_2023D[np.where(bin_digits_2023D == i)]) for i in range(len(bins))])
            
        efficiency_2023C = numerator_2023C / denominator_2023C
        efficiency_2023D = numerator_2023D / denominator_2023D
        
        lower_error_2023C, upper_error_2023C = getError(numerator_2023C, denominator_2023C)
        lower_error_2023D, upper_error_2023D = getError(numerator_2023D, denominator_2023D)

        # ymax = np.min((1., np.max(efficiency_2023D[np.isfinite(efficiency_2023D)]) * 1.2))
        ymax = 0.65

        bin_centers = np.diff(np.concatenate((bins, [bins[-1]+(bins[-1]-bins[-2])])))/2 + bins
        
        plot_efficiencies(
            [efficiency_2023C, efficiency_2023D],
            [lower_error_2023C, lower_error_2023D],
            [upper_error_2023C, upper_error_2023D],
            plot_text,
            bin_centers,
            labels=['pre-BPix','post-BPix'],
            plot_config = plot_config,
            title=None,
            path=os.path.join(args.out, "eff_npv_{}.png".format(path)),
            xlabel="Number of reconstructed primary vertices",
            ymax=ymax,
            lumi = lumi_str
        )
        
        plot_efficiencies(
            [efficiency_2023C, efficiency_2023D],
            [lower_error_2023C, lower_error_2023D],
            [upper_error_2023C, upper_error_2023D],
            plot_text,
            bin_centers,
            labels=['pre-BPix','post-BPix'],
            plot_config = plot_config,
            title=None,
            path=os.path.join(args.out, "eff_npv_{}_log.png".format(path)),
            xlabel="number of primary vertices",
            ymax=ymax,
            log=True,
            lumi = lumi_str
        )      
        
        plot_efficiencies(
            [efficiency_2023C, efficiency_2023D],
            [lower_error_2023C, lower_error_2023D],
            [upper_error_2023C, upper_error_2023D],
            plot_text,
            bin_centers,
            labels=['pre-BPix','post-BPix'],
            plot_config = plot_config,
            title=None,
            path=os.path.join(args.out, "eff_npv_{}.pdf".format(path)),
            xlabel="Number of offline reconstructed primary vertices",
            ymax=ymax,
            lumi = lumi_str
        )
        
        plot_efficiencies(
            [efficiency_2023C, efficiency_2023D],
            [lower_error_2023C, lower_error_2023D],
            [upper_error_2023C, upper_error_2023D],
            plot_text,
            bin_centers,
            labels=['pre-BPix','post-BPix'],
            plot_config = plot_config,
            title=None,
            path=os.path.join(args.out, "eff_npv_{}_log.pdf".format(path)),
            xlabel="Number of offline reconstructed primary vertices",
            ymax=ymax,
            log=True,
            lumi = lumi_str
        )
        
#         plot_efficiency(
#             efficiency_2023C,
#             lower_error_2023C,
#             upper_error_2023C,
#             plot_text,
#             bin_centers,
#             label = None,
#             title = None,
#             ymax=ymax,
#             path=os.path.join(args.out, "2023_eff_npv_{}.png".format(seq)), 
#             xlabel="NPV",
#             lumi = r"$3.055\,\mathrm{fb}^{-1}, 2022, 26.6\,\mathrm{fb}^{-1}, 2023$ ($13.6\,\mathrm{TeV}$)",
#         )
        
#         plot_efficiency(
#             efficiency_2023C,
#             lower_error_2023C,
#             upper_error_2023C,
#             plot_text,
#             bin_centers,
#             label = None,
#             title = None,
#             ymax=ymax,
#             log=True,
#             path=os.path.join(args.out, "2023_eff_npv_{}_log.png".format(seq)), 
#             xlabel="NPV",
#             lumi = r"$3.055\,\mathrm{fb}^{-1}, 2022, 26.6\,\mathrm{fb}^{-1}, 2023$ ($13.6\,\mathrm{TeV}$)",
#         )

        ##### plot in leading jet phi
        
        bins = np.linspace(-3, 3, num=10-1, endpoint=False)
        bin_digits_2023C = np.digitize(jetphi_2023C, bins)
        print(bin_digits_2023C)
        bin_digits_2023D = np.digitize(jetphi_2023D, bins)

        numerator_2023C = np.array([np.sum(n_passing_2023C[np.where(bin_digits_2023C == i)]) for i in range(len(bins))])
        numerator_2023D = np.array([np.sum(n_passing_2023D[np.where(bin_digits_2023D == i)]) for i in range(len(bins))])
        
        denominator_2023C = np.array([np.sum(n_total_2023C[np.where(bin_digits_2023C == i)]) for i in range(len(bins))])
        denominator_2023D = np.array([np.sum(n_total_2023D[np.where(bin_digits_2023D == i)]) for i in range(len(bins))])
        
        efficiency_2023C = numerator_2023C/denominator_2023C
        efficiency_2023D = numerator_2023D/denominator_2023D
        
        lower_error_2023C, upper_error_2023C = getError(numerator_2023C, denominator_2023C)
        lower_error_2023D, upper_error_2023D = getError(numerator_2023D, denominator_2023D)

        bin_centers = np.diff(np.concatenate((bins, [1.])))/2 + bins
        plot_title = path[:len(path) //2 ] + "\n" + path[len(path) //2 :]
        ymax = np.min((1., np.max(efficiency_2023C[np.isfinite(efficiency_2023C)]) * 1.2))
        ymax = 0.6
        
        plot_efficiencies(
                [efficiency_2023C, efficiency_2023D], 
                [lower_error_2023C, lower_error_2023D],
                [upper_error_2023C, upper_error_2023D],
                plot_text,
                bin_centers,
                labels=['pre-BPix','post-BPix'],
                plot_config = plot_config,
                title = None, 
                xmin=-3,
                xmax= 3,
                ymax=ymax,
                lumi = lumi_str,
                path=os.path.join(args.out, "eff_jet_phi_{}.png".format(seq)), 
                xlabel="$\phi$ of AK4 jet with {0} leading offline b-tagging score".format(index_str)
            
        )
        
        plot_efficiencies(
                [efficiency_2023C, efficiency_2023D],
                [lower_error_2023C, lower_error_2023D],
                [upper_error_2023C, upper_error_2023D],
                plot_text,
                bin_centers,
                labels=['pre-BPix','post-BPix'],
                plot_config = plot_config,
                title=None,
                xmin=-3,
                xmax= 3,
                path=os.path.join(args.out, "eff_jet_phi_{}_log.png".format(seq)),
                xlabel="$\phi$ of AK4 jet with {0} leading offline b-tagging score".format(index_str),
                ymax=ymax,
                lumi = lumi_str,
                log=True
        )
        
        plot_efficiencies(
                [efficiency_2023C, efficiency_2023D], 
                [lower_error_2023C, lower_error_2023D],
                [upper_error_2023C, upper_error_2023D],
                plot_text,
                bin_centers,
                labels=['pre-BPix','post-BPix'],
                plot_config = plot_config,
                title = None, 
                xmin=-3,
                xmax= 3,
                ymax=ymax,
                lumi = lumi_str,
                path=os.path.join(args.out, "eff_jet_phi_{}.pdf".format(seq)), 
                xlabel="$\phi$ of AK4 jet with {0} leading offline b-tagging score".format(index_str)
            
        )
        
        plot_efficiencies(
                [efficiency_2023C, efficiency_2023D],
                [lower_error_2023C, lower_error_2023D],
                [upper_error_2023C, upper_error_2023D],
                plot_text,
                bin_centers,
                labels=['pre-BPix','post-BPix'],
                plot_config = plot_config,
                title=None,
                xmin=-3,
                xmax= 3,
                path=os.path.join(args.out, "eff_jet_phi_{}_log.pdf".format(seq)), 
                xlabel="$\phi$ of AK4 jet with {0} leading offline b-tagging score".format(index_str),
                ymax=ymax,
                lumi = lumi_str,
                log=True
        )
        
#         plot_efficiency(
#             efficiency_2023C,
#             lower_error_2023C,
#             upper_error_2023C,
#             plot_text,
#             bin_centers,
#             label = None,
#             title = None,
#             ymax=ymax,
#             path=os.path.join(args.out, "2023_eff_jet_phi_{}.png".format(seq)), 
#             xlabel="offline {} b-tag phi".format(index),
#             lumi = r"$3.055\,\mathrm{fb}^{-1}, 2022, 26.6\,\mathrm{fb}^{-1}, 2023$ ($13.6\,\mathrm{TeV}$)",
#         )
        
#         plot_efficiency(
#             efficiency_2023C,
#             lower_error_2023C,
#             upper_error_2023C,
#             plot_text,
#             bin_centers,
#             label = None,
#             title = None,
#             ymax=ymax,
#             log=True,
#             path=os.path.join(args.out, "2023_eff_jet_phi_{}_log.png".format(seq)), 
#             xlabel="offline {} b-tag phi".format(index),
#             lumi = r"$3.055\,\mathrm{fb}^{-1}, 2022, 26.6\,\mathrm{fb}^{-1}, 2023$ ($13.6\,\mathrm{TeV}$)",
#         )
        
        ##### plot in rec lumi 
        
#         bins_2022 = np.linspace(np.min(rec_lumi_2022), np.max(rec_lumi_2022), 20)
#         bins_2022 = np.concatenate( (np.array([-1.]), bins))
        
#         bins_2023 = np.linspace(np.min(rec_lumi_2023), np.max(rec_lumi_2023), 20)
#         bins_2023 = np.concatenate( (np.array([-1.]), bins))
        
#         # bin_digits = np.digitize(avg_inst_lumi, bins)
#         bin_digits_2022 = np.digitize(rec_lumi_2022, bins_2022)
#         bin_digits_2023 = np.digitize(rec_lumi_2023, bins_2023)

#         numerator_2022 = np.array([np.sum(n_passing_2022[np.where(bin_digits_2022 == i)]) for i in range(len(bins_2022))])
#         denominator_2022 = np.array([np.sum(n_total_2022[np.where(bin_digits_2022 == i)]) for i in range(len(bins_2022))])
        
#         numerator_2023 = np.array([np.sum(n_passing_2023[np.where(bin_digits_2023 == i)]) for i in range(len(bins_2023))])
#         denominator_2023 = np.array([np.sum(n_total_2023[np.where(bin_digits_2023 == i)]) for i in range(len(bins_2023))])

#         efficiency_2022 = numerator_2022 / denominator_2022
#         efficiency_2023 = numerator_2023 / denominator_2023
#         lower_error_2022, upper_error_2022 = getError(numerator_2022, denominator_2022)
#         lower_error_2023, upper_error_2023 = getError(numerator_2023, denominator_2023)

#         ymax_2022 = np.max(efficiency_2022[np.isfinite(efficiency_2022)]) * 1.2
#         ymax_2023 = np.max(efficiency_2023[np.isfinite(efficiency_2023)]) * 1.2

# #         plot_title = path[:len(path) //2 ] + "\n" + path[len(path) //2 :]
#         bin_centers_2022 = np.diff(np.concatenate((bins_2022, [bins_2022[-1]+(bins_2022[-1]-bins_2022[-2])])))/2 + bins_2022
#         bin_centers_2022[0] = 0.
        
#         bin_centers_2023 = np.diff(np.concatenate((bins_2023, [bins_2023[-1]+(bins_2023[-1]-bins_2023[-2])])))/2 + bins_2023
#         bin_centers_2023[0] = 0.
        
#         # plot_efficiency(efficiency, lower_error, upper_error, bin_centers, title = None, path=os.path.join(args.out, f"eff_inst_lumi_{path}.png"), xlabel="avg. inst. Luminosity /$(\mu b s)$",ymax=ymax)
# #         plot_efficiency(efficiency, lower_error, upper_error, bin_centers, title = None, path=os.path.join(args.out, f"eff_inst_lumi_{path}_log.png"), xlabel="avg. inst. Luminosity /$(\mu b s)$", log=True,ymax=ymax)

#         plot_efficiencies(
#             [efficiency_2023, efficiency_2022],
#             [lower_error_2023, lower_error_2022],
#             [upper_error_2023, upper_error_2022],
#             bin_centers_2022,
#             labels=['Run2023C_v3','Run2022G'],
#             title=plot_title,
#             path=os.path.join(args.out, "eff_inst_lumi_{}.png".format(path)),
#             xlabel="avg. inst. Luminosity /$(\mu b s)$",
#             ymax=ymax
#         )
        
#         plot_efficiencies(
#             [efficiency_2023, efficiency_2022],
#             [lower_error_2023, lower_error_2022],
#             [upper_error_2023, upper_error_2022],
#             bin_centers_2022,
#             labels=['Run2023C_v3','Run2022G'],
#             title=plot_title,
#             path=os.path.join(args.out, "eff_inst_lumi_{}_log.png".format(path)),
#             xlabel="avg. inst. Luminosity /$(\mu b s)$",
#             ymax=ymax,
#             log=True
#         )   
"""
