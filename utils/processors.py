import hist
import json
import awkward as ak
import numpy as np
from coffea import processor, lumi_tools
from coffea.processor.accumulator import (
    AccumulatorABC,
    column_accumulator,
    defaultdict_accumulator,
    dict_accumulator,
    set_accumulator,
)
from typing import List, Dict
from selections import ttbar_selection


class SequenceProcessor(processor.ProcessorABC):
    def __init__(self, bins: int = 20, sequences: List[str] = None, selections: Dict = None, merge_datasets=None):
        """

        Args:
            sequences (List[str], optional): Trigger Bit sequences to analyze.
            selections (Dict, optional): Selections to apply. This should be a dictionary of the form {str: callable -> mask}.
        """

        self.bins = bins

        self.sequences = sequences
        self.selections = {}
        self.selections.update(selections)
        if merge_datasets is not None:
            self.merge_datasets = merge_datasets
        else:
            self.merge_datasets = {}

    def create_hists(self, **kwargs):
        hists = (
            hist.Hist.new.StrCat(self.sequences, name="sequence")
            .StrCat(self.selections.keys(), name="selection")
            .Regular(self.bins, 0.0, 1.0, name="n_passing", label="Offline b-tag value")
            .Double()
        )
        return hists

    def process(self, events):

        dataset = events.metadata["dataset"]
        print(dataset)
        print(len(events))
        hists = self.create_hists()
        """
        select only events that hate the Trigger objects
        """
        events["selTrigObj"] = events.TrigObj[events.TrigObj.id == 1]
        events = events[ak.num(events.selTrigObj) > 0]
        """
        add different selections
        """
        analysis_selections = processor.PackedSelection()
        for selection_name, selection_callable in self.selections.items():
            analysis_selections.add(selection_name, selection_callable(events))

        # find closest jet
        events["selTrigObj", "Jet"] = events.selTrigObj.nearest(events.Jet)
        events["selTrigObj", "Jet", "dr"] = events.selTrigObj.Jet.delta_r(events.selTrigObj)

        # decode self.sequences into filter
        for i, sequence in enumerate(self.sequences):
            events["selTrigObj", sequence] = (events.selTrigObj.filterBits & 1 << i) > 0

        """
        fill histograms
        """
        for sequence in self.sequences:
            for selection_name in analysis_selections.names:
                selected = events.selTrigObj.Jet.btagDeepFlavB[events["selTrigObj", sequence]][
                    analysis_selections.require(**{selection_name: True})
                ]
                hists.fill(
                    sequence=sequence,
                    selection=selection_name,
                    n_passing=ak.flatten(selected).to_numpy(),
                )
        """
        in case you want to save some arrays, do like demonstrated below:

        trigSeq = {
            sequence: column_accumulator(
                ak.flatten(
                    events.selTrigObj.Jet.btagDeepFlavB[events["selTrigObj", sequence]]
                ).to_numpy()
            )
            for sequence in self.sequences
        }
        """

        ret = {dataset: {}}
        ret[dataset]["entries"] = len(events)
        ret[dataset]["sequence_hist"] = hists

        for merge_set in self.merge_datasets.keys():
            ret[merge_set] = {}
        for merge_set, merge_datasets in self.merge_datasets.items():
            if dataset in merge_datasets:
                ret[merge_set]["entries"] = len(events)
                ret[merge_set]["sequence_hist"] = hists
        # ret[dataset].update(trigSeq)

        return ret

    def postprocess(self, accumulator):
        pass


class ExportProcessor(processor.ProcessorABC):
    def __init__(self, paths=None, sequences=None, PU_file=None, Lumi_mask_dict=None ):
        """

        Args:
            sequences (List[str], optional): Trigger Bit sequences to analyze.
            selections (Dict, optional): Selections to apply. This should be a dictionary of the form {str: callable -> mask}.
        """
        self.paths = paths
        self.sequences = sequences
        if PU_file is not None:
            """
            PU file is strucured like this:
                - LS number
                - recorded luminosity (/μb)
                - RMS bunch instantaneous luminosity/orbit frequency (/μb/bunch)
                - average bunch instantaneous luminosity/orbit frequency (/μb/bunch)
            see:
            https://twiki.cern.ch/twiki/bin/viewauth/CMS/PileupJSONFileforData
            """
            try:
                with open(PU_file,"r") as PU_file_json:
                    self.PU_dict = json.load(PU_file_json)
            except FileNotFoundError as e:
                print("PU_file could not be opened!\nMake sure that you do not try to access Lumi information on the run or LS.")
            self.PU_dict = {run_number: {block[0]: block[1:] for block in luminosity_blocks} for run_number, luminosity_blocks in self.PU_dict.items()}
        if Lumi_mask_dict is not None:
            self.Lumi_mask_func_dict = {}
            for RunEra_, Lumi_mask_file_ in Lumi_mask_dict.items():          
                self.Lumi_mask_func_dict[RunEra_] = lumi_tools.LumiMask(Lumi_mask_file_)
        super().__init__()

    @property
    def get_recorded_luminosity(self):
        return np.vectorize(self._get_recorded_luminosity)

    def _get_recorded_luminosity(self, run, LS):
        try:
            return self.PU_dict[str(run)][LS][0]
        except IndexError as e:
            print(e)
            print(f"IndexError in run: {run} and ls: {LS}")
            return 0.
        except KeyError as e:
            print(e)
            print(f"KeyError in run: {run} and ls: {LS}")
            return 0.

    def process(self, events):

        dataset = events.metadata["dataset"]
        # only select events that have the trigger object
        events["selTrigObj"] = events.TrigObj[events.TrigObj.id == 1]
        events = events[ak.num(events.selTrigObj) > 0]
        
        # # apply ttbar-selection
        ttbar_mask = ttbar_selection(events)
        events = events[ttbar_mask]
                        
        if len(self.Lumi_mask_func_dict) != 0 and dataset in self.Lumi_mask_func_dict.keys():
            events["Lumi_Mask"] = self.Lumi_mask_func_dict[dataset](events.run, events.luminosityBlock)
            events = events[events["Lumi_Mask"]]
            
        # print("after lumi_mask")
        # print(events)

        # find closest jet
        # events["selTrigObj", "Jet"] = events.selTrigObj.nearest(events.Jet)
        # events["selTrigObj", "Jet", "dr"] = events.selTrigObj.Jet.delta_r(events.selTrigObj)

        # decode self.sequences into filter
        # for i, sequence in enumerate(self.sequences):
        #     events["selTrigObj", sequence] = (events.selTrigObj.filterBits & 1 << i) > 0
        """
        get b-tag values as well as target/refernce-path values
        """
        ret = {}

        # for sequence in self.sequences:
        #     selected = ak.fill_none(ak.max(events.selTrigObj.Jet.btagDeepFlavB[events["selTrigObj", sequence]], axis=-1), -1)
        #     ret[sequence] = column_accumulator(selected.to_numpy())

        for path in self.paths:
            # print(path)
            ret[path] = column_accumulator(getattr(events.HLT, path).to_numpy())

        ret["npvs"] = column_accumulator(events.PV.npvs.to_numpy())
        
        # jet_pt_pad = ak.pad_none(events.Jet.pt, 3, axis=1)
        # jet_pt_pad_none = ak.fill_none(jet_pt_pad, -99.)
        # max_pt_indices = []
        # for _jet in jet_pt_pad_none:
        #     max_pt_indices.append(np.argmax(np.array(_jet)))
            
        
#         max_phi_values = []
        
#         _ijet = 0
#         for _jet in jet_phi_pad_none:
#             max_phi_values.append(_jet[max_pt_indices[_ijet]])
#             _ijet += 1
#         ret["jetphi"] = column_accumulator(np.array(max_phi_values))
        
        b_pad = ak.pad_none(events.Jet.btagDeepFlavB, 3, axis=1)
        b_pad_none = ak.fill_none(b_pad, -99.)
        sorted_b = ak.sort(b_pad_none, axis=-1)
        ret["btags"] = column_accumulator(sorted_b[..., -3:].to_numpy())
        print(sorted_b[..., -3:])
        
        sorted_indices = []
        for _jet in sorted_b:
            sorted_indices.append(np.argsort(_jet, axis=-1))
        
        jet_phi_pad = ak.pad_none(events.Jet.phi, 3, axis=1)
        jet_phi_pad_none = ak.fill_none(jet_phi_pad, -99.)
        sorted_jet_phi = []
        _ijet = 0
        for _idx in sorted_indices:
            sorted_jet_phi.append([ jet_phi_pad_none[_ijet][_idx[0]], jet_phi_pad_none[_ijet][_idx[1]], jet_phi_pad_none[_ijet][_idx[2]] ])
            _ijet += 1
        # sorted_jet_phi = ak.values(jet_phi_pad_none, axis=1)[:, sorted_indices]
        sorted_jet_phi = ak.Array(sorted_jet_phi)
        ret["jetphi"] = column_accumulator(sorted_jet_phi[..., -3:].to_numpy())
        print(sorted_jet_phi[..., -3:])
        
        # ret["recorded_lumi"] = column_accumulator(self.get_recorded_luminosity(events.run, events.luminosityBlock))

        return ret

    def postprocess(self, accumulator):
        pass

class TriggerReviewProcessor(processor.ProcessorABC):
    def __init__(self, paths=None, PU_file=None, merge_datasets=None, Lumi_mask_dict=None):
        """

        Args:
            sequences (List[str], optional): Trigger Bit sequences to analyze.
            selections (Dict, optional): Selections to apply. This should be a dictionary of the form {str: callable -> mask}.
        """
        self.paths = paths
        if merge_datasets is not None:
            self.merge_datasets = merge_datasets
        else:
            self.merge_datasets = {}

        if PU_file is not None:
            """
            PU file is strucured like this:
                - LS number
                - recorded luminosity (/μb)
                - RMS bunch instantaneous luminosity/orbit frequency (/μb/bunch)
                - average bunch instantaneous luminosity/orbit frequency (/μb/bunch)
            see:
            https://twiki.cern.ch/twiki/bin/viewauth/CMS/PileupJSONFileforData
            """
            try:
                with open(PU_file,"r") as PU_file_json:
                    self.PU_dict = json.load(PU_file_json)
            except FileNotFoundError as e:
                print("PU_file could not be opened!\nMake sure that you do not try to access Lumi information on the run or LS.")
            self.PU_dict = {run_number: {block[0]: block[1:] for block in luminosity_blocks} for run_number, luminosity_blocks in self.PU_dict.items()}
            
        if Lumi_mask_dict is not None:
            self.Lumi_mask_func_dict = {}
            for RunEra_, Lumi_mask_file_ in Lumi_mask_dict.items():          
                self.Lumi_mask_func_dict[RunEra_] = lumi_tools.LumiMask(Lumi_mask_file_)
        super().__init__()

    @property
    def get_recorded_luminosity(self):
        return np.vectorize(self._get_recorded_luminosity)

    def _get_recorded_luminosity(self, run, LS):
        try:
            return self.PU_dict[str(run)][LS][0]
        except IndexError as e:
            print(e)
            print(f"IndexError in run: {run} and ls: {LS}")
            return 0.
        except KeyError as e:
            print(e)
            print(f"KeyError in run: {run} and ls: {LS}")
            return 0.

    def process(self, events):

        dataset = events.metadata["dataset"]

        # only select events that have the trigger object
        events["selTrigObj"] = events.TrigObj[events.TrigObj.id == 1]
        events = events[ak.num(events.selTrigObj) > 0]

        # # apply ttbar-selection
        ttbar_mask = ttbar_selection(events)
        events = events[ttbar_mask]
        
        if len(self.Lumi_mask_func_dict) != 0:
            events["Lumi_Mask"] = self.Lumi_mask_func_dict[dataset](events.run, events.luminosityBlock)
            events = events[events["Lumi_Mask"]]

        ret = {dataset: {}}
        for path in self.paths:
            ret[dataset][path] = column_accumulator(getattr(events.HLT, path).to_numpy())

        # sorted_b = ak.sort(ak.fill_none(ak.pad_none(events.Jet.btagDeepFlavB, 3, axis=1), -99.), axis=-1)

        ret[dataset]["npvs"] = column_accumulator(events.PV.npvs.to_numpy())
        
        b_pad = ak.pad_none(events.Jet.btagDeepFlavB, 3, axis=1)
        b_pad_none = ak.fill_none(b_pad, -99.)
        sorted_b = ak.sort(b_pad_none, axis=-1)
        ret["btags"] = column_accumulator(sorted_b[..., -3:].to_numpy())
        
        sorted_indices = []
        for _jet in sorted_b:
            sorted_indices.append(np.argsort(_jet, axis=-1))
        
        jet_phi_pad = ak.pad_none(events.Jet.phi, 3, axis=1)
        jet_phi_pad_none = ak.fill_none(jet_phi_pad, -99.)
        sorted_jet_phi = []
        _ijet = 0
        for _idx in sorted_indices:
            sorted_jet_phi.append([ jet_phi_pad_none[_ijet][_idx[0]], jet_phi_pad_none[_ijet][_idx[1]], jet_phi_pad_none[_ijet][_idx[2]] ])
            _ijet += 1
        # sorted_jet_phi = ak.values(jet_phi_pad_none, axis=1)[:, sorted_indices]
        sorted_jet_phi = ak.Array(sorted_jet_phi)
        ret["jetphi"] = column_accumulator(sorted_jet_phi[..., -3:].to_numpy())
        
        ret[dataset]["recorded_lumi"] = column_accumulator(self.get_recorded_luminosity(events.run, events.luminosityBlock))

        for merge_set in self.merge_datasets.keys():
            ret[merge_set] = {}
        for merge_set, merge_datasets in self.merge_datasets.items():
            if dataset in merge_datasets:
                ret[merge_set]["npvs"] = column_accumulator(events.PV.npvs.to_numpy())
                ret[merge_set]["btags"] = column_accumulator(sorted_b[..., -3:].to_numpy())
                ret[merge_set]["jetphi"] = column_accumulator(sorted_jet_phi[..., -3:].to_numpy())
                ret[merge_set]["recorded_lumi"] = column_accumulator(self.get_recorded_luminosity(events.run, events.luminosityBlock))
                for path in self.paths:
                    ret[merge_set][path] = column_accumulator(getattr(events.HLT, path).to_numpy())

        return ret

    def postprocess(self, accumulator):
        pass