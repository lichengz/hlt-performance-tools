import matplotlib
import matplotlib.pyplot as plt
import mplhep
import numpy as np
from collections.abc import KeysView

matplotlib.use("Agg")
plt.style.use(mplhep.style.CMS)


def plot(
    hists,
    sigmas,
    bins=None,
    labels=None,
    path=None,
    title=None,
    xmin=None,
    xmax=None,
    xlabel=None,
    normalize=False,
    lumi="$13.6\,$TeV",
    sort=True,
):
    fig = plt.figure(figsize=(10, 10))
    plot = fig.add_subplot()

    mplhep.cms.label(loc=0, ax=plot, data=True, rlabel=lumi)

    kwargs = {}
    if bins is not None:
        kwargs["bins"] = bins

    if sort and isinstance(labels, (list, KeysView)):
        labels = list(labels)
        sorting = np.argsort(labels)
        hists = [hists[s] for s in sorting]
        sigmas = [sigmas[s] for s in sorting]
        labels = [labels[s] for s in sorting]

    mplhep.histplot(
        # hists, yerr=sigmas, ax=plot, histtype="errorbar", density=normalize, label=labels, **kwargs
        hists, yerr=sigmas, ax=plot, histtype="errorbar", density=normalize, **kwargs
    )

    plot.legend(loc=2)
    plot.set_title(title, y=1.1)
    plot.set_xlabel(xlabel)
    plot.set_xlim(xmin, xmax)
    plot.set_ylabel(
        "number of jets {}".format("(normalized)" if normalize else ""), verticalalignment="bottom"
    )

    fig.tight_layout()
    fig.savefig(path, dpi=300)
    plt.close()

def plot_efficiency(
    efficiency,
    sigma_down,
    sigma_up,
    multiline_text,
    bin_centers=None,
    labels=None,
    path=None,
    title=None,
    label=None,
    xmin=None,
    xmax=None,
    ymax=1,
    xlabel=None,
    normalize=False,
    lumi = r"$26.6\,\mathrm{fb}^{-1}$ ($13.6\,\mathrm{TeV}$)",
    plot_config=None,
    sort=True,
    **kwargs
):
    fig, plot = plt.subplots(figsize=(10, 10))

    mplhep.cms.label("Preliminary", loc=0, ax=plot, data=True, rlabel=lumi)
    
    if plot_config is None:
        plot_config = {}

    plot.errorbar(
        bin_centers, efficiency, yerr=[sigma_down, sigma_up], marker=plot_config.get(label, {}).get("marker", "o"), ls='none',color=plot_config.get(label, {}).get("color", None), capsize=3
    )

    plot.legend(loc=2)
    plot.set_title(title, y=1.1)
    plot.set_xlabel(xlabel)
    plot.set_xlim(xmin, xmax)
    plot.set_ylabel(
        "Efficiency", verticalalignment="bottom"
    )
    if kwargs.get("log", False):
        plot.set_yscale("log")
    plot.set_ylim(0., ymax)
    plot.text(0.05, 0.85, multiline_text, fontsize=16, color='black', transform=plot.transAxes)
    # plot.text(2, 5, 'multiline_text', fontsize=6, color='black')
    fig.tight_layout()
    fig.savefig(path, dpi=300)
    plt.close()

def plot_efficiencies(
    efficiencies,
    sigmas_down,
    sigmas_up,
    multiline_text=None,
    bin_centers=None,
    labels=None,
    path=None,
    title=None,
    xmin=None,
    xmax=None,
    ymax=1,
    xlabel=None,
    normalize=False,
    lumi = r"$26.6\,\mathrm{fb}^{-1}$ ($13.6\,\mathrm{TeV}$)",
    sort=True,
    plot_config=None,
    **kwargs
):
    # fig, plot = plt.subplots(figsize=(10, 10))
    
    fig, (ax_main, ax_ratio) = plt.subplots(nrows=2, ncols=1, sharex=True, gridspec_kw={'height_ratios': [3, 1]})

    mplhep.cms.label("Preliminary", loc=0, ax=ax_main, data=True, rlabel=lumi, fontsize=18)
    
    if plot_config is None:
        plot_config = {}

    # if sort and isinstance(labels, (list, KeysView)):
    #     labels = list(labels)
    #     sorting = np.argsort(labels)
    #     hists = [hists[s] for s in sorting]
    #     sigmas = [sigmas[s] for s in sorting]
    #     labels = [labels[s] for s in sorting]

    main_plot_colors = []
    for i, (efficiency, sigma_down, sigma_up, label) in enumerate(zip(efficiencies, sigmas_down, sigmas_up, labels)):
        offset = np.min(np.diff(bin_centers)) / 13 * i
        ax_main.errorbar(
            bin_centers + offset, efficiency, yerr=[sigma_down, sigma_up], marker=plot_config.get(label, {}).get("marker", "o"), ls='none', color=plot_config.get(label, {}).get("color", None), capsize=3, label=label
        )
        main_plot_colors.append(plot_config.get(label, {}).get("color", None))
       
    ratios = []
    ratio_errors_down = []
    ratio_errors_up = []
    
    for i, (efficiency, sigma_down, sigma_up, label) in enumerate(zip(efficiencies, sigmas_down, sigmas_up, labels)):
        ratios.append(efficiency / efficiencies[1])
        ratio_errors_down.append(sigma_down / efficiencies[1])
        ratio_errors_up.append(sigma_up / efficiencies[1])
        
    for _iplot in range(0,len(ratios)-1):
        ax_ratio.errorbar(bin_centers, ratios[_iplot], yerr=[ratio_errors_down[_iplot], ratio_errors_up[_iplot]], marker='o', ls='none', color=main_plot_colors[_iplot], capsize=3)

    print(main_plot_colors)
    ax_ratio.axhline(1.0, color=main_plot_colors[1], linestyle='--')  # Add a horizontal line at y=1 for reference
    ax_ratio.set_ylim(0.0, 3.0)  # Set the y-axis limits for the ratio plot
    
    # ax_main.legend(facecolor='white', framealpha=1., loc='best')
    ax_main.legend(facecolor='white', framealpha=1., loc='lower right')
    ax_main.set_title(title, y=1.1)
    ax_main.set_xlabel(xlabel)
    ax_main.set_xlim(xmin, xmax)
    ax_main.set_ylabel(
        "Efficiency", verticalalignment="bottom"
    )
    if kwargs.get("log", False):
        ax_main.set_yscale("log")
    ax_main.set_ylim(0., ymax)
    
    ax_ratio.set_ylabel('Ratio')  # Set the label for the ratio plot y-axis

    ax_ratio.legend(loc='upper left', fontsize=12)
    # ax_main.text(0.05, 0.80, multiline_text, fontsize=16, color='black', transform=ax_main.transAxes)
    ax_main.text(0.15, 0.05, multiline_text, fontsize=16, color='black', transform=ax_main.transAxes)
    fig.tight_layout()
    fig.savefig(path, dpi=300)
    plt.close()
    
def plot_histo(
    efficiencies,
    sigma_down,
    sigma_up,
    bin_centers=None,
    labels=None,
    path=None,
    title=None,
    label=None,
    xmin=None,
    xmax=None,
    ymax=1,
    xlabel=None,
    ylabel=None,
    normalize=False,
    lumi="$13.6\,$TeV",
    plot_config=None,
    sort=True,
    **kwargs
):
    fig, plot = plt.subplots(figsize=(10, 10))

    mplhep.cms.label("Preliminary", loc=0, ax=plot, data=True, rlabel=lumi)

    if plot_config is None:
        plot_config = {}
    
    plot.errorbar(
        bin_centers, efficiencies, yerr=[sigma_down, sigma_up], marker=plot_config.get(label, {}).get("marker", "o"), ls='none',color=plot_config.get(label, {}).get("color", None), capsize=3
    )

    plot.legend(loc=2)
    plot.set_title(title, y=1.1)
    plot.set_xlabel(xlabel)
    plot.set_xlim(xmin, xmax)
    plot.set_ylabel(ylabel)
    if kwargs.get("log", False):
        plot.set_yscale("log")
    plot.set_ylim(0., ymax)

    fig.tight_layout()
    fig.savefig(path, dpi=300)
    plt.close()