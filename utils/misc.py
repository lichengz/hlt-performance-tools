from functools import reduce
from operator import and_, or_, add


def reduce_and(*what):
    return reduce(and_, what)


def reduce_or(*what):
    return reduce(or_, what)


def setup_fileset(input_paths, labels, maxFiles=None):
    if not (isinstance(input_paths, list)):
        input_paths = [input_paths]
        labels = [labels]
    fileset = {}
    for path, label in zip(input_paths, labels):
        if path.endswith(".txt"):
            with open(path, "r") as txt_file_list:
                file_list = list(map(lambda x: x.rstrip("\n"), txt_file_list.readlines()))
        elif path.endswith(".root"):
            file_list = [path]
        else:
            raise NotImplementedError
        fileset[label] = file_list[0 : None if not maxFiles else min(maxFiles, len(file_list))]
    return fileset
