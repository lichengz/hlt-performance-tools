#!/bin/bash

# BTagMu
data='data/BTagMu_Run2022C-PromptReco-v1_fill8102.root'
reference='data/DQM_Offline_Run2018_BTagMu_R0003200xx_S0005.root'
# reference='data/BTagMu_Run2022C-PromptReco-v1_fill8033.root'

# SingleMu
# data='data/SingleMuon_Run2022C-PromptReco-v1_fill8033.root'
# reference='data/DQM_V0001_R000315420__SingleMuon__Run2018A-ForValUL2018-v1__DQMIO.root'




plot()
{
    category=$1
    trigger=$2
    echo "plotting '$trigger' in $category"


    # bjet
    python plot_DQM.py --label "bjet Pt" --out plots/$category/$trigger-bjetPt.pdf \
    --path $data --refpath $reference \
    --numerator BTV/$category/$trigger/bjetPt_1_numerator \
    --denominator BTV/$category/$trigger/bjetPt_1_denominator &

    python plot_DQM.py --label "bjet Eta" --out plots/$category/$trigger-bjetEta.pdf \
    --path $data --refpath $reference \
    --numerator BTV/$category/$trigger/bjetEta_1_numerator \
    --denominator BTV/$category/$trigger/bjetEta_1_denominator &

    python plot_DQM.py --label "bjet Phi" --out plots/$category/$trigger-bjetPhi.pdf \
    --path $data --refpath $reference \
    --numerator BTV/$category/$trigger/bjetPhi_1_numerator \
    --denominator BTV/$category/$trigger/bjetPhi_1_denominator &

    python plot_DQM.py --label "bjet multiplicity" --out plots/$category/$trigger-bjetmulti.pdf \
    --path $data --refpath $reference \
    --numerator BTV/$category/$trigger/bjetMulti_numerator \
    --denominator BTV/$category/$trigger/bjetMulti_denominator &

    python plot_DQM.py --label "bjet CSV" --out plots/$category/$trigger-bjetCSV.pdf \
    --path $data --refpath $reference \
    --numerator BTV/$category/$trigger/bjetCSV_1_numerator \
    --denominator BTV/$category/$trigger/bjetCSV_1_denominator &



    #     muon
    python plot_DQM.py --label "muon Pt" --out plots/$category/$trigger-MuPt.pdf \
    --path $data --refpath $reference \
    --numerator BTV/$category/$trigger/muPt_1_numerator \
    --denominator BTV/$category/$trigger/muPt_1_denominator &

    python plot_DQM.py --label "muon Eta" --out plots/$category/$trigger-MuEta.pdf \
    --path $data --refpath $reference \
    --numerator BTV/$category/$trigger/muEta_1_numerator \
    --denominator BTV/$category/$trigger/muEta_1_denominator &

    python plot_DQM.py --label "muon Phi" --out plots/$category/$trigger-MuPhi.pdf \
    --path $data --refpath $reference \
    --numerator BTV/$category/$trigger/muPhi_1_numerator \
    --denominator BTV/$category/$trigger/muPhi_1_denominator &

    python plot_DQM.py --label "muon multiplicity" --out plots/$category/$trigger-Mumulti.pdf \
    --path $data --refpath $reference \
    --numerator BTV/$category/$trigger/muMulti_numerator \
    --denominator BTV/$category/$trigger/muMulti_denominator &



    python plot_DQM.py --label "DeltaR(jet, muon)" --out plots/$category/$trigger-DetlaRJetMu.pdf \
    --path $data --refpath $reference \
    --numerator BTV/$category/$trigger/DeltaR_jet_Mu_numerator \
    --denominator BTV/$category/$trigger/DeltaR_jet_Mu_denominator &

    wait
}


plot BTagDiMu_Jet BTagMu_AK8Jet170_DoubleMu5

for trigger in BTagMu_AK4Jet300_Mu5 BTagMu_AK8Jet300_Mu5
do
    plot BTagMu_Jet $trigger
done

for trigger in BTagMu_AK4DiJet20_Mu5 BTagMu_AK4DiJet40_Mu5 BTagMu_AK4DiJet70_Mu5 BTagMu_AK4DiJet110_Mu5 BTagMu_AK4DiJet170_Mu5 BTagMu_AK8DiJet170_Mu5
do
    plot BTagMu_DiJet $trigger
done
